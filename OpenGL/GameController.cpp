#include "GameController.h"
#include "WindowController.h"
#include "ToolWindow.h"

GameController::GameController()
{
	m_shaderColor = { };
	m_shaderDiffuse = { };
	m_camera = { };
	m_meshBoxes.clear();
}

GameController::~GameController()
{
}

void GameController::Initialize()
{
	GLFWwindow* window = WindowController::GetInstance().GetWindow();
	M_ASSERT(glewInit() == GLEW_OK, "Failed to initialize GLEW");
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glClearColor(0.1f, 0.1f, 0.1, 0.0f);
	//glClearColor(0.0f, 0.0f, 0.0, 0.0f);
	//glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	srand(time(0));

	m_camera = Camera(WindowController::GetInstance().GetResolution());
}

void GameController::RunGame()
{
	m_shaderColor = Shader();
	m_shaderColor.LoadShaders("Color.vert", "Color.frag");
	m_shaderDiffuse = Shader();
	m_shaderDiffuse.LoadShaders("Diffuse.vert", "Diffuse.frag");

	for (int count = 0; count < 4; count++)
	{
		Mesh m = Mesh();
		m.Create(&m_shaderColor);
		//m.SetPosition({ 1.0f, -0.5f, 0.0f });
		//m.SetPosition({ 0.5f, 0.0f, -0.5f });
		m.SetPosition({ 0.5f + (float)count / 10.0f, 0.0f, -0.5f });
		m.SetColor({ glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f) });
		m.SetScale({ 0.1f, 0.1f, 0.1f });
		Mesh::Lights.push_back(m);
	}

	for (int col = 0; col < 10; col++)
	{
		for (int count = 0; count < 10; count++)
		{
			Mesh box = Mesh();

			box.Create(&m_shaderDiffuse);
			//box.SetLightColor({ 1.0f, 1.0f, 1.0f });
			//box.SetLightPosition(m_meshLight.GetPosition());
			box.SetCameraPosition(m_camera.GetPosition());
			box.SetScale({ 0.1f, 0.1f, 0.1f });
			//box.SetPosition({ glm::linearRand(-1.0f, 1.0f), glm::linearRand(-1.0f, 1.0f), glm::linearRand(-1.0f, 1.0f) });
			//box.SetPosition({ 0, count, 0 });
			box.SetPosition({ 0.0, -0.5 + (float)count / 10.0f,-0.2f + (float)col / 10.0f });

			m_meshBoxes.push_back(box);
		}
	}

	GLFWwindow* win = WindowController::GetInstance().GetWindow();
	do
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		for (auto& box : m_meshBoxes)
		{
			box.Render(m_camera.GetProjection() * m_camera.GetView());
		}

		for (auto& light : Mesh::Lights)
		{
			light.Render(m_camera.GetProjection() * m_camera.GetView());
		}

		glfwSwapBuffers(win);
		glfwPollEvents();

	} while (glfwGetKey(win, GLFW_KEY_ESCAPE) != GLFW_PRESS
		&& glfwWindowShouldClose(win) == 0);

	for (auto& box : m_meshBoxes)
	{
		box.Cleanup();
	}

	for (auto& light : Mesh::Lights)
	{
		light.Cleanup();
	}

	m_shaderDiffuse.Cleanup();
	m_shaderColor.Cleanup();
}
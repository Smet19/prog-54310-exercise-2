#version 330

in vec3 vertices;
in vec4 colors;
in vec2 texCoords;

uniform mat4 WVP;

out vec4 retColor;
out vec2 retTexCoord;

void main()
{
	gl_Position = WVP * vec4(vertices, 1.0f);
	retColor = colors;
	retTexCoord = texCoords;
}
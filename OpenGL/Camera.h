#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "StandardIncludes.h"

class Camera
{
public:
	// Constructors / Destructors
	Camera();
	Camera(Resolution _screenResolution);
	virtual ~Camera();

	glm::mat4 GetProjection() { return m_projection; }
	glm::mat4 GetView() { return m_view; }
	glm::vec3 GetPosition() { return m_position; }

private:
	glm::mat4 m_projection;
	glm::mat4 m_view;
	glm::vec3 m_position;

};
#endif // _CAMERA_H_


#version 330

in vec3 retNormal;
in vec2 retTexCoord;
in vec3 retFragPos;
in vec3 retViewDirection;

out vec4 FragColor;

struct Material
{
	float specularStrength;
	sampler2D diffuseTexture;
	sampler2D specularTexture;
};

struct Light
{
	vec3 position;
	vec3 color;
	vec3 ambientColor;
	vec3 diffuseColor;
	vec3 specularColor;

	float constant;
	float linear;
	float quadratic;

	vec3 direction;
	float coneAngle;
	float falloff;
};

uniform Material material;
//uniform Light light;

#define NR_LIGHTS 4
uniform Light light[NR_LIGHTS];


void main()
{
	vec4 finalColor = vec4(0);

	for(int i = 0; i < NR_LIGHTS; i++)
	{
		vec3 lightDir = normalize(light[i].position - retFragPos);
		float a = cos(light[i].coneAngle);
		float d = dot(normalize(light[i].direction), -lightDir);
		if( a < d )
		{
			//vec3 lightDir = light[i].position - vec3(0, 0, 0);
			vec3 lambertianStrength = dot(retNormal, lightDir) * light[i].color;
			vec3 refl = reflect(-lightDir, retNormal);
			float specularStrength = pow(max(dot(refl, retViewDirection), 0.0f), material.specularStrength);
			float distance = length(light[i].position - retFragPos);
			float att = 1.0f / (light[i].constant + light[i].linear * distance + light[i].quadratic * (distance * distance));
			att *= 1 - pow(clamp(a / d, 0.0f, 1.0f), light[i].falloff);

			vec3 ambient = texture(material.diffuseTexture, retTexCoord).rgb * light[i].ambientColor * att / NR_LIGHTS;
			vec3 lambertian = lambertianStrength * texture(material.diffuseTexture, retTexCoord).rgb * light[i].diffuseColor * att;
			vec3 specular = specularStrength * texture(material.specularTexture, retTexCoord).rgb * light[i].specularColor * att;
			finalColor += vec4(ambient + lambertian + specular, 1.0f);
		}
		else
		{
			vec3 ambient = texture(material.diffuseTexture, retTexCoord).rgb * light[i].ambientColor / NR_LIGHTS;
			finalColor += vec4(ambient, 1.0f);
		}
	}

	FragColor = finalColor;
}